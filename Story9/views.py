from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

def index(request):
    context = {
        'page_title': 'Beranda'
        }
    indexpage = None
    if request.user.is_authenticated:
        indexpage = 'index_user.html'
    else:
        indexpage = 'index_anon.html'

    return render(request, indexpage, context)

def loginView(request):
    context = {
        'page_title': 'Log in'
        }
    
    if request.method == "POST":
        username_input = request.POST['username']
        password_input = request.POST['password']

        user = authenticate(request, username=username_input, password=password_input)

        if user is not None:
            login(request, user)
            return redirect('index')
        else:
            return render(request, 'login.html', {'alert_flag':  True})
            
    return render(request, 'login.html', context)

def logoutView(request):
    context = {
        'page_title': 'Log Out'
        }
    if request.method == "POST":
        if request.POST["logout"] == 'Submit':
            logout(request)
            return redirect('index')

    return render(request, 'logout.html', context)

def signupView(request):
    context = {
        'page_title': 'Sign Up'
        }
    if request.method == "POST":
        username_set = request.POST['username']
        password_set = request.POST['password']

        if username_set and password_set is not None:
            user = authenticate(request, username=username_set, password=password_set)
            
            if user is None:
                usercreate = User.objects.create_user(username=username_set, password=password_set)
                usercreate.save()
                return redirect('login')
            else:
                return render(request, 'signup.html', {'alert_flag':  True})
    return render(request, 'signup.html', context)

    
    
